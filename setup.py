from distutils.core import setup

setup(
    name='ForcaMP',
    version='1.0',
    packages=['ForcaMP.View', 'ForcaMP.Model', 'ForcaMP.Controller'],
    url='',
    license='BSD',
    author='victorgv',
    author_email='victorgvbh@gmail.com',
    description='',
    requires=['Pillow', 'pygobject', 'requests']
)
