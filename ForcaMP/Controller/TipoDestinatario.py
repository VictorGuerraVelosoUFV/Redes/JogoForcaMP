#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import Enum


class TipoDestinatario(Enum):
    VISITANTE = 0
    HOSPEDEIRO = 1
    AMBOS = 2
