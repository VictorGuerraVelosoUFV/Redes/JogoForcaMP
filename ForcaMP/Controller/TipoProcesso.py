#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import Enum


class TipoProcesso(Enum):
    CLIENTE = 0
    SERVIDOR = 1
