#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

from ForcaMP.Controller.TipoProcesso import TipoProcesso
from ForcaMP.Model.EstadoPartida import EstadoPartida
from ForcaMP.Model.Partida import Partida

from ForcaMP.Model.Jogador import Jogador

from ForcaMP.Controller.GerenciadorPartida import GerenciadorPartida
from ForcaMP.Controller.IPC import IPC
from ForcaMP.Model.TipoParticipante import TipoParticipante
from requests import get


class ControladorServidor:
    def __init__(self):
        self._iPC = IPC()
        self._gerenciadorPartida = GerenciadorPartida()

    @property
    def iPC(self):
        return self._iPC

    @iPC.setter
    def iPC(self, value):
        assert isinstance(value, IPC)
        self._iPC = value

    @property
    def gerenciadorPartida(self):
        return self._gerenciadorPartida

    @gerenciadorPartida.setter
    def gerenciadorPartida(self, value):
        assert isinstance(value, GerenciadorPartida)
        self._gerenciadorPartida = value

    def criarPartida(self, nome, tema, vidaMax, hospedeiro):
        self.gerenciadorPartida.criarPartida(nome, self._sortearPalavra(tema), tema, self.obterIP(), vidaMax,
                                             hospedeiro)

    def obterIP(self):
        return get('https://api.ipify.org').text

    def consumirMensagem(self):
        self._iPC.estabelecerConexao(TipoProcesso.SERVIDOR)
        msg = self.iPC.receberMensagem()
        assert isinstance(msg, str)
        tipo = msg.split(']', 1)[0][1:]  # [...]
        remetente = msg.split('(', 1)[1].split(')', 1)[0]  # (...)
        if '{' in msg:
            cmd = msg.split(')', 1)[1].split('{', 1)[0]  # )...{
            parametros = msg.split('{', 1)[1][:-1].split(',')  # {...}
        else:
            cmd = msg.split(')', 1)[1]
            parametros = []
        if tipo == 'action':
            if cmd == 'conectarPartida' and len(parametros) == 4:
                if remetente == 'visitante':
                    self.gerenciadorPartida.conectarPartida(Jogador(parametros[0], int(parametros[3])))
                elif remetente == 'hospedeiro':
                    for participante in self.gerenciadorPartida.obterPartidaCorrente().participantes:
                        if participante.tipo == TipoParticipante.HOSPEDEIRO:
                            assert parametros[0] == participante.nome
                            assert int(parametros[3]) == participante.nivel
                            assert participante.tipo == TipoParticipante.HOSPEDEIRO
            elif cmd == 'escolherLetra' and len(parametros) == 1:
                if remetente == 'visitante':
                    if self.gerenciadorPartida.obterPartidaCorrente().estado == EstadoPartida.VISITANTE:
                        self._mostrarLetra(parametros[0], TipoParticipante.VISITANTE)
                    else:
                        self._informarEquivoco("Não é sua rodada, aguarde seu adversário")
                elif remetente == 'hospedeiro':
                    if self.gerenciadorPartida.obterPartidaCorrente().estado == EstadoPartida.HOSPEDEIRO:
                        self._mostrarLetra(parametros[0], TipoParticipante.HOSPEDEIRO)
                    else:
                        self._informarEquivoco("Não é sua rodada, aguarde seu adversário")
        elif tipo == 'update':
            if cmd == 'confirmarConexao' and len(parametros) == 1:
                if remetente == 'visitante':
                    pass  # TODO: Notificar jogadores de que a partida começou
            elif cmd == 'encerrarConexao' and len(parametros) == 0:
                if remetente == 'visitante':
                    self._desligarServidor()  # TODO: Implementar retomada à partida (queda conexão)
                elif remetente == 'hospedeiro':
                    self._desligarServidor()
        self._iPC.desfazerConexao()

    def _sortearPalavra(self, tema):
        assert isinstance(tema, str)
        try:
            with open(tema) as f:
                texto = f.read().splitlines()
        except IOError:
            print("Não existe esse arquivo")
            return None
        return random.choice(texto)

    def _mostrarLetra(self, letra, jogador):
        assert isinstance(jogador, TipoParticipante)
        assert isinstance(letra, str) and len(letra) == 1
        palavra = self.gerenciadorPartida.obterPartidaCorrente().palavra
        palavraVisivel = [' '] * palavra.tamanho
        acerto = False
        for letra_indice in palavra.letraIndice.values():
            if letra_indice.letra.caractere == letra:
                acerto = True
                self._aumentarPontuacao(jogador)
                if letra_indice.letra.escolhida:
                    self._informarEquivoco("Letra já escolhida anteriormente")
                else:
                    letra_indice.letra.escolhida = True
                for idx in letra_indice.indices:
                    palavraVisivel[idx] = letra_indice.letra.caractere
            else:
                if letra_indice.letra.escolhida:
                    for idx in letra_indice.indices:
                        palavraVisivel[idx] = letra_indice.letra.caractere
        if not acerto:
            self._descontarVida()

        self.iPC.enviarMensagem(
            "[update](servidor)informarEscolha{" + str(int(acerto)) + "," + letra + "," + palavraVisivel + "," + str(
                self.gerenciadorPartida.obterPartidaCorrente().vida))

    def _aumentarPontuacao(self, jogador):
        assert isinstance(jogador, TipoParticipante)
        total_pontos = []
        for participante in self.gerenciadorPartida.obterPartidaCorrente().participantes:
            if participante.tipo == jogador:
                participante.pontuacao.valor += 1
            total_pontos.append((participante.pontuacao.valor, participante.tipo))
        if (total_pontos[0][0] + total_pontos[1][0]) >= len(
                self.gerenciadorPartida.obterPartidaCorrente().palavra.letraIndice.values()):
            self.iPC.enviarMensagem('[update](servidor)fimPartida{0,' + max(total_pontos,
                                                                            key=lambda x: x[0])[1] + '}')

    def _descontarVida(self):
        partida = self.gerenciadorPartida.obterPartidaCorrente()
        vida = partida.vida
        vida.atual -= 1
        if vida.atual == 0:
            self.iPC.enviarMensagem('[update](servidor)fimPartida{0,' + str(
                max(partida.participantes, key=lambda x: x.pontuacao.valor).tipo) + '}')

    def _informarEquivoco(self, msg):
        self.iPC.enviarMensagem('[warning](servidor)informarEquivoco{' + msg + '}')

    def _desligarServidor(self):
        pass
