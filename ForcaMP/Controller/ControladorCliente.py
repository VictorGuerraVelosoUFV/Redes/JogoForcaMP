#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Jogador import Jogador
from ForcaMP.Controller.GerenciadorPartida import GerenciadorPartida
from ForcaMP.Controller.IPC import IPC


class ControladorCliente:
    def __init__(self):
        self._iPC = None
        self._gerenciadorPartida = GerenciadorPartida()
        self._jogador = Jogador()

    @property
    def iPC(self):
        return self._iPC

    @iPC.setter
    def iPC(self, value):
        assert isinstance(value, IPC)
        self._iPC = value

    @property
    def jogador(self):
        return self._jogador

    @jogador.setter
    def jogador(self, value):
        assert isinstance(value, Jogador)
        self._jogador = value

    @property
    def gerenciadorPartida(self):
        return self._gerenciadorPartida

    @gerenciadorPartida.setter
    def gerenciadorPartida(self, value):
        assert isinstance(value, GerenciadorPartida)
        self._gerenciadorPartida = value

    def entrarPartida(self, IP):
        pass

    def alterarIcone(self, caminho):
        pass

    def alterarNome(self, nome):
        pass

    def escolherLetra(self, letra):
        pass

    def alternarSom(self):
        pass

    def abortarPartida(self):
        pass

    def verHistorico(self):
        pass

    def desenharTela(self):
        pass

    def _esconderMenu(self):
        pass

    def _verJogo(self):
        pass

    def _esconderJogo(self):
        pass

    def _verMenu(self):
        pass

    def _esperarAdversario(self):
        pass