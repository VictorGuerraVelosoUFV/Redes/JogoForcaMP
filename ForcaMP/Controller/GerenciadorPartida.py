#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.EstadoPartida import EstadoPartida
from ForcaMP.Model.Partida import Partida

from ForcaMP.Model.Jogador import Jogador


class GerenciadorPartida:
    def __init__(self):
        self._partida = []

    def criarPartida(self, nome, palavra, tema, ip, vida_max, hospedeiro):
        assert isinstance(nome, str)
        assert isinstance(palavra, str)
        assert isinstance(tema, str)
        assert isinstance(ip, str)
        assert isinstance(vida_max, int)
        assert isinstance(hospedeiro, Jogador)
        self.partida.append(Partida(nome, tema, ip, palavra, vida_max, hospedeiro))

    def obterPartidaCorrente(self):
        for partida in reversed(self.partida):
            if partida.emAndamento:
                return partida
        raise RuntimeError("Nenhuma partida está em andamento.")

    def conectarPartida(self, visitante):
        assert isinstance(visitante, Jogador)
        falhou = True
        for partida in reversed(self.partida):
            if partida.estado == EstadoPartida.ESPERA:
                falhou = False
                try:
                    partida.ingressoVisitante(visitante)
                except RuntimeError:
                    falhou = True
        if falhou:
            raise RuntimeError("Nenhuma partida válida encontrada.")

    def obterHistorico(self, jogador):
        assert isinstance(jogador, Jogador)
        historico = []
        for partida in self.partida:
            if any(participante.nome == jogador.nome for participante in partida.participantes):
                historico.append(partida)
        return historico

    @property
    def partida(self):
        return self._partida

    @partida.setter
    def partida(self, value):
        assert isinstance(value, list) and all(isinstance(item, Partida) for item in value)
        self._partida = value
