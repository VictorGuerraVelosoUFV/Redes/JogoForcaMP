#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket

from ForcaMP.Controller.TipoProcesso import TipoProcesso
from ForcaMP.Controller.TipoDestinatario import TipoDestinatario


class IPC:
    def __init__(self, ip=None):
        self._IP = ip
        self._tipo = None
        self._skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._skt.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._connection = {}
        self._destinatario = None

    @property
    def IP(self):
        return self._IP

    @IP.setter
    def IP(self, value):
        assert isinstance(value, str)
        self._IP = value

    @property
    def skt(self):
        return self._skt

    @skt.setter
    def skt(self, value):
        assert isinstance(value, socket.socket)
        self._skt = value

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, value):
        assert isinstance(value, socket.socket)
        self._connection = value

    @property
    def destinatario(self):
        return self._destinatario

    @destinatario.setter
    def destinatario(self, value):
        assert isinstance(value, tuple) and all(isinstance(item, str) for item in value)
        self._destinatario = value

    def enviarMensagem(self, mensagem, destinatario=TipoDestinatario.AMBOS):
        if self._tipo == TipoProcesso.CLIENTE:
            self.skt.sendall(mensagem.encode('utf-8'))
        elif self._tipo == TipoProcesso.SERVIDOR:
            if destinatario == TipoDestinatario.HOSPEDEIRO:
                self.connection[TipoDestinatario.HOSPEDEIRO].sendall(mensagem.encode('utf-8'))
            elif destinatario == TipoDestinatario.VISITANTE:
                self.connection[TipoDestinatario.VISITANTE].sendall(mensagem.encode('utf-8'))
            else:
                for c in self.connection.values():
                    c.sendall(mensagem.encode('utf-8'))
        self.skt.shutdown(1)

    def receberMensagem(self, destinatario=TipoDestinatario.AMBOS, data=list()):
        assert isinstance(data, list)
        msg = None
        if self._tipo == TipoProcesso.CLIENTE:
            msg = self.skt.recv(1024)
        elif self._tipo == TipoProcesso.SERVIDOR:
            if destinatario == TipoDestinatario.VISITANTE:
                msg = self.connection[TipoDestinatario.VISITANTE].recv(1024)
            else:
                msg = self.connection[TipoDestinatario.HOSPEDEIRO].recv(1024)
        if not msg:
            return ''.join(data)
        else:
            data.append(msg.decode("utf-8", "strict"))
            print(data)
            return self.receberMensagem(destinatario, data)

    def estabelecerConexao(self, tipo, destinatario=TipoDestinatario.HOSPEDEIRO):
        assert isinstance(tipo, TipoProcesso)
        self._tipo = tipo
        if tipo == TipoProcesso.CLIENTE:
            self.skt.connect((self.IP, 25566))
        elif tipo == TipoProcesso.SERVIDOR:
            if len(self._connection) == 0:
                self.skt.bind(('', 25566))
                self.skt.listen(5)
            if destinatario == TipoDestinatario.VISITANTE or len(self._connection) == 1:
                self.connection[TipoDestinatario.VISITANTE], address = self.skt.accept()
            else:
                self.connection[TipoDestinatario.HOSPEDEIRO], address = self.skt.accept()
            self.IP = address[0]

    def desfazerConexao(self, destinatario=TipoDestinatario.AMBOS):
        if self._tipo == TipoProcesso.CLIENTE:
            self.skt.close()
        elif self._tipo == TipoProcesso.SERVIDOR:
            if destinatario == TipoDestinatario.HOSPEDEIRO:
                self.connection[TipoDestinatario.HOSPEDEIRO].close()
            elif destinatario == TipoDestinatario.VISITANTE:
                self.connection[TipoDestinatario.VISITANTE].close()
            else:
                for c in self.connection.values():
                    c.close()
        self._tipo = None


if __name__ == "__main__":
    entrada = input()
    if entrada == 's':
        ipc = IPC()
        ipc.estabelecerConexao(TipoProcesso.SERVIDOR)
        print(ipc.connection)
        ipc.estabelecerConexao(TipoProcesso.SERVIDOR)
        print(ipc.connection)
        print(ipc.receberMensagem(TipoDestinatario.VISITANTE))
        ipc.enviarMensagem('tchau', TipoDestinatario.VISITANTE)
        print(ipc.receberMensagem(TipoDestinatario.HOSPEDEIRO))
        ipc.enviarMensagem('banana', TipoDestinatario.HOSPEDEIRO)
        '''
        while True:
            dialogo = ipc.receberMensagem()
            print(dialogo)
            if dialogo == 'oi':
                ipc.enviarMensagem('tchau')
            elif dialogo == 'fim':
                break
        ipc.desfazerConexao()
        '''
        ipc.desfazerConexao()
    elif entrada == 'c':
        ipc = IPC('127.0.0.1')
        ipc.estabelecerConexao(TipoProcesso.CLIENTE)
        ent = input()
        ipc.enviarMensagem(ent)
        dialogo = ipc.receberMensagem()
        print(dialogo)
        ipc.desfazerConexao()
    else:
        ipc = IPC('127.0.0.1')
        ipc.estabelecerConexao(TipoProcesso.CLIENTE)
        ent = input()
        ipc.enviarMensagem(ent)
        dialogo = ipc.receberMensagem()
        print(dialogo)
        ipc.desfazerConexao()
