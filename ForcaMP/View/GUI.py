#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from ForcaMP.View.GameView import GameView

class GUI:
    def __init__(self):
        self._builder = Gtk.Builder()
        self._builder.add_from_file("GUI.glade")
        self._configChanges = set()
        self._setUpWindow()
        self._setUpSignals()

    def _rodarJogo(self):
        self._mw.hide()
        gv = GameView()
        gv.run()


    def _cancelarConfig(self):
        self._configChanges.clear()
        self._screens.set_current_page(0)

    def _salvarConfig(self):
        if 'h' in self._configChanges:      # historico
            self._hm.clear()
        elif 'i' in self._configChanges:    # icone
            pass
        elif 'n' in self._configChanges:    # nome
            pass
        self._cancelarConfig()
        self._screens.set_current_page(0)

    def _setUpSignals(self):
        self._builder.connect_signals({"onDestroy": Gtk.main_quit,
                                       "abrirConfiguracao": lambda x: self._screens.set_current_page(6),
                                       "abrirHistorico": lambda x: self._screens.set_current_page(5),
                                       "reiniciarHistorico": lambda x: self._configChanges.add('h'),  # TODO: Limpar do banco
                                       "salvarConfig": lambda x: self._salvarConfig(),
                                       # TODO: Aplicar alterações (sync bd)
                                       "cancelarConfig": lambda x: self._cancelarConfig(),
                                       "abrirCriarPartida": lambda x: self._screens.set_current_page(1),
                                       "abrirEntrarPartida": lambda x: self._screens.set_current_page(2),
                                       "abrirEsperaPartida": lambda x: self._screens.set_current_page(3),
                                       # TODO: Fazer mais do que isso
                                       "abrirConfirmacaoPartida": lambda x: self._screens.set_current_page(4),
                                       # TODO: Fazer mais do que isso
                                       "voltarMainScreen": lambda x: self._screens.set_current_page(0),
                                       "hideGtk": lambda x: self._rodarJogo()})

    def _setUpWindow(self):
        self._mw = self._builder.get_object("MainWindow")
        self._cw = self._builder.get_object("ConfigWindow")
        self._hw = self._builder.get_object("HistoricoWindow")
        self._screens = self._builder.get_object("Screens")
        self._hm = self._builder.get_object("HistoricoModelo")

    def show(self):
        self._mw.show_all()
        Gtk.main()
