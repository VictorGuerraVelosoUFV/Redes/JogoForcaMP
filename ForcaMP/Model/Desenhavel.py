#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Desenhavel(metaclass=ABCMeta):
    def __init__(self):
        self._posicao = None
        self._tamanho = None

    @property
    def posicao(self):
        return self._posicao

    @posicao.setter
    def posicao(self, value):
        assert isinstance(value, tuple) and all(isinstance(eixo, float) for eixo in value)
        self._posicao = value

    @property
    def tamanho(self):
        return self._tamanho

    @tamanho.setter
    def tamanho(self, value):
        assert isinstance(value, tuple) and all(isinstance(dim, float) for dim in value)
        self._tamanho = value

    @abstractmethod
    def filho(self):
        pass
