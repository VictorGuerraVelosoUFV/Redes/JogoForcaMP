#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL.Image import Image
from PIL.Image import open as imgopen


class Jogador:
    def __init__(self, nome="", nivel=0, icone=imgopen("ForcaMP/assets/defaultUser.png")):
        assert isinstance(nome, str)
        assert isinstance(nivel, int)
        assert isinstance(icone, Image)
        self._nome = nome
        self._nivel = nivel
        self._icone = icone

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, value):
        assert isinstance(value, str)
        self._nome = value

    @property
    def nivel(self):
        return self._nivel

    @nivel.setter
    def nivel(self, value):
        assert isinstance(value, int)
        self._nivel = value

    @property
    def icone(self):
        return self._icone

    @icone.setter
    def icone(self, value):
        assert isinstance(value, Image)
        self._icone = value
