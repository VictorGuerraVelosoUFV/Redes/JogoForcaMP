#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import Enum


class TipoParticipante(Enum):
    VISITANTE = 0
    HOSPEDEIRO = 1
