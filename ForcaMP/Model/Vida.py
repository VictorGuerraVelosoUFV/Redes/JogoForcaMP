#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel


class Vida(Desenhavel):
    def __init__(self, maxima):
        super().__init__()
        assert isinstance(maxima, int)
        self._atual = maxima
        self._maxima = maxima

    @property
    def atual(self):
        return self._atual

    @atual.setter
    def atual(self, value):
        assert isinstance(value, int)
        self._atual = value

    @property
    def maxima(self):
        return self._maxima

    @maxima.setter
    def maxima(self, value):
        assert isinstance(value, int)
        self._maxima = value

    def filho(self):
        return []
