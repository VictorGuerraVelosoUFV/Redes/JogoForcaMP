#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel
from ForcaMP.Model.Jogador import Jogador
from ForcaMP.Model.Participante import Participante
from ForcaMP.Model.TipoParticipante import TipoParticipante
from ForcaMP.Model.Vida import Vida
from ForcaMP.Model.Palavra import Palavra
from ForcaMP.Model.Alfabeto import Alfabeto
from ForcaMP.Model.Rodada import Rodada
from ForcaMP.Model.EstadoPartida import EstadoPartida
from build.lib.Model.Tema import Tema


class Partida(Desenhavel):
    def __init__(self, nome, tema, ip, palavra, vida, hospedeiro, alfabeto=Alfabeto(), **kwargs):
        super().__init__()
        assert isinstance(nome, str)
        assert isinstance(tema, str)
        assert isinstance(ip, str)
        assert isinstance(palavra, Palavra)
        assert isinstance(vida, Vida)
        assert isinstance(hospedeiro, Jogador)
        assert isinstance(alfabeto, Alfabeto)
        self._nome = nome
        if kwargs["caminho_tema"]:
            self._tema = Tema(tema, kwargs["caminho_tema"])
        else:
            self._tema = Tema(tema)
        self._IP = ip
        self._vencida = False
        self._emAndamento = False
        self._palavra = palavra
        self._vida = vida
        self._alfabeto = alfabeto
        self._participantes = [Participante(jogador=hospedeiro, tipo=TipoParticipante.HOSPEDEIRO)]
        self._rodadas = []
        self._estado = EstadoPartida.ESPERA

    @property
    def vencida(self):
        return self._vencida

    @vencida.setter
    def vencida(self, value):
        assert isinstance(value, bool)
        self._vencida = value

    @property
    def emAndamento(self):
        return self._emAndamento

    @emAndamento.setter
    def emAndamento(self, value):
        assert isinstance(value, bool)
        self._emAndamento = value

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, value):
        assert isinstance(value, str)
        self._nome = value

    @property
    def tema(self):
        return self._tema

    @tema.setter
    def tema(self, value):
        assert isinstance(value, str)
        self._tema = value

    @property
    def IP(self):
        return self._IP

    @IP.setter
    def IP(self, value):
        assert isinstance(value, str)
        self._IP = value

    @property
    def palavra(self):
        return self._palavra

    @palavra.setter
    def palavra(self, value):
        assert isinstance(value, Palavra)
        self._palavra = value

    @property
    def vida(self):
        return self._vida

    @vida.setter
    def vida(self, value):
        assert isinstance(value, Vida)
        self._vida = value

    @property
    def alfabeto(self):
        return self._alfabeto

    @alfabeto.setter
    def alfabeto(self, value):
        assert isinstance(value, Alfabeto)
        self._alfabeto = value

    @property
    def estado(self):
        return self._estado

    @estado.setter
    def estado(self, value):
        assert isinstance(value, EstadoPartida)
        self._estado = value

    @property
    def participantes(self):
        return self._participantes

    def ingressoVisitante(self, jogador):
        assert isinstance(jogador, Jogador)
        if self.estado != EstadoPartida.ESPERA:
            raise RuntimeError("Partida já começou!")
        visitante = Participante(jogador=jogador, tipo=TipoParticipante.VISITANTE)
        self._participantes.append(visitante)
        self.estado = EstadoPartida.VISITANTE
        self.emAndamento = True
        self._rodadas.append(Rodada(visitante))

    def egressoVisitante(self, jogador):
        assert isinstance(jogador, Jogador)
        if self.estado == EstadoPartida.ESPERA:
            raise RuntimeError("Partida não começou!")
        for participante in self.participantes:
            if participante.nome == jogador.nome:
                self._participantes.remove(participante)
        self.estado = EstadoPartida.ESPERA
        self.emAndamento = False

    def proximaRodada(self):
        if self.estado == EstadoPartida.HOSPEDEIRO:
            self.estado = EstadoPartida.VISITANTE
            self._rodadas.append(Rodada(self._participantes[1]))
        elif self.estado == EstadoPartida.VISITANTE:
            self.estado = EstadoPartida.HOSPEDEIRO
            self._rodadas.append(Rodada(self._participantes[0]))
        else:
            return None
        return self._rodadas[-2]

    def filho(self):
        return [self.tema, self.palavra, self.vida, self.alfabeto, self.participantes[0], self.participantes[1]]
