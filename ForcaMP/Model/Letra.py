#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel


class Letra(Desenhavel):
    def __init__(self, caractere):
        super().__init__()
        assert isinstance(caractere, str) and len(caractere) == 1
        self._caractere = caractere
        self._cor = (0, 0, 0)
        self._escolhida = False

    @property
    def caractere(self):
        return self._caractere

    @caractere.setter
    def caractere(self, value):
        assert isinstance(value, str) and len(value) == 1
        self._caractere = value

    @property
    def cor(self):
        return self._cor

    @cor.setter
    def cor(self, value):
        assert isinstance(value, tuple) and all(isinstance(espectro, int) for espectro in value)
        self._cor = value

    def filho(self):
        return []

    @property
    def escolhida(self):
        return self._escolhida

    @escolhida.setter
    def escolhida(self, value):
        assert isinstance(value, bool)
        self._escolhida = value
