#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel
from ForcaMP.Model.Letra import Letra


class Alfabeto(Desenhavel):
    def __init__(self, letras=list()):
        super().__init__()
        assert isinstance(letras, list) and all(isinstance(letra, Letra) for letra in letras)
        self._letras = letras
        if not letras:
            for i in range(ord('A'), ord('Z') + 1):
                self._letras.append(Letra(chr(i)))
            for i in range(ord('a'), ord('z') + 1):
                self._letras.append(Letra(chr(i)))

    def __add__(self, other):
        assert isinstance(other, Letra)
        letras = self._letras.copy()
        letras.append(other)
        return Alfabeto(letras)

    def __iadd__(self, other):
        assert isinstance(other, Letra)
        self._letras.append(other)

    @property
    def letras(self):
        return self._letras

    @letras.setter
    def letras(self, value):
        assert isinstance(value, list) and all(isinstance(letra, Letra) for letra in value)
        self._letras = value

    def filho(self):
        return self.letras
