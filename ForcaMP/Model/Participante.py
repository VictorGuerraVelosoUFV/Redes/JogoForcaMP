#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel
from ForcaMP.Model.Pontuacao import Pontuacao
from ForcaMP.Model.Jogador import Jogador
from ForcaMP.Model.TipoParticipante import TipoParticipante


class Participante(Jogador, Desenhavel):
    def __init__(self, **kwargs):
        if kwargs["nome"] and kwargs["nivel"] and kwargs["icone"]:
            super().__init__(kwargs["nome"], kwargs["nivel"], kwargs["icone"])
        elif kwargs["jogador"]:
            super().__init__(kwargs["jogador"].nome, kwargs["jogador"].nivel, kwargs["jogador"].icone)
        else:
            raise ValueError('Invalid keyword parameters! Got "' + str(
                kwargs.keys()) + '", expected "jogador" or ("nome" and "nivel" and "icone")!')
        if kwargs["pontuacao"]:
            assert isinstance(kwargs["pontuacao"], Pontuacao)
            pontuacao = kwargs["pontuacao"]
        else:
            pontuacao = Pontuacao()
        if kwargs["tipo"]:
            assert isinstance(kwargs["tipo"], TipoParticipante)
            self._tipo = kwargs["tipo"]
        else:
            raise ValueError('Player type parameter missing!')
        self._pontuacao = pontuacao

    @property
    def pontuacao(self):
        return self._pontuacao

    @pontuacao.setter
    def pontuacao(self, value):
        assert isinstance(value, Pontuacao)
        self._pontuacao = value

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, value):
        assert isinstance(value, TipoParticipante)
        self._tipo = value

    def filho(self):
        return [self.pontuacao]
