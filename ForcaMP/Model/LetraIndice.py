#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Letra import Letra


class LetraIndice:
    def __init__(self, letra, indices=list()):
        assert isinstance(indices, list) and all(isinstance(idx, int) for idx in indices)
        assert isinstance(letra, Letra)
        self._letra = letra
        self._indices = indices[:]

    def __iter__(self):
        for idx in self.indices:
            yield idx

    def __iadd__(self, other):
        assert isinstance(other, int)
        self.indices.append(other)

    def __add__(self, other):
        assert isinstance(other, int)
        idx = self.indices[:]
        idx.append(other)
        return LetraIndice(idx)

    @property
    def indices(self):
        return self._indices

    @indices.setter
    def indices(self, value):
        assert isinstance(value, list) and all(isinstance(idx, int) for idx in value)
        self._indices = value[:]

    @property
    def letra(self):
        return self._letra

    @letra.setter
    def letra(self, value):
        assert isinstance(value, Letra)
        self._letra = value
