#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import Enum


class EstadoPartida(Enum):
    ESPERA = 0
    VISITANTE = 1
    HOSPEDEIRO = 2
    VITORIA = 3
    DERROTA = 4
