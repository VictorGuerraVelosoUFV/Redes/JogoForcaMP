#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Letra import Letra
from ForcaMP.Model.Participante import Participante


class Rodada:
    def __init__(self, participante):
        assert isinstance(participante, Participante)
        self._participante = participante
        self._letra = None

    @property
    def letra(self):
        return self._letra

    @letra.setter
    def letra(self, value):
        assert isinstance(value, Letra)
        self._letra = value

    @property
    def participante(self):
        return self._participante

    @participante.setter
    def participante(self, value):
        assert isinstance(value, Participante)
        self._participante = value
