#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel
from ForcaMP.Model.Palavra import Palavra
from random import shuffle


class Tema(Desenhavel):
    def __init__(self, nome, caminho="./"):
        super().__init__()
        assert isinstance(nome, str)
        assert isinstance(caminho, str)
        self._palavras = []
        self._nome = nome
        with open(caminho + nome) as arquivo:
            linha = arquivo.readline()
            if len(linha) > 1:
                self.palavras.append(Palavra(linha[:-1]))

    @property
    def palavras(self):
        return self._palavras

    @palavras.setter
    def palavras(self, value):
        assert isinstance(value, list) and all(isinstance(palavra, Palavra) for palavra in value)
        self._palavras = value

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, value):
        assert isinstance(value, str)
        self._nome = value

    def sortearPalavra(self):
        shuffle(self._palavras)
        return self._palavras[0]

    def filho(self):
        return []
