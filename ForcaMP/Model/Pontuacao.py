#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel


class Pontuacao(Desenhavel):
    def __init__(self, valor=0):
        super().__init__()
        assert isinstance(valor, int)
        self._valor = valor

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, value):
        assert isinstance(value, int)
        self._valor = value

    def filho(self):
        return []
