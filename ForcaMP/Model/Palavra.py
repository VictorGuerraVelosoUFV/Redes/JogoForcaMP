#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ForcaMP.Model.Desenhavel import Desenhavel
from ForcaMP.Model.LetraIndice import LetraIndice
from ForcaMP.Model.Letra import Letra


class Palavra(Desenhavel):
    def __init__(self, palavra):
        super().__init__()
        assert isinstance(palavra, str)
        self._tamanho = len(palavra)
        self._letraIndice = {}
        uniqueLetra = set()
        for idx, letra in enumerate(palavra):
            if not letra in uniqueLetra:
                uniqueLetra.add(letra)
                self._letraIndice[letra] = LetraIndice([idx])
            else:
                self._letraIndice[letra] += idx

    def obterIndices(self, letra):
        if isinstance(letra, str) and len(letra) == 1:
            return self._letraIndice[letra]
        elif isinstance(letra, Letra):
            return self._letraIndice[letra.caractere]
        else:
            raise ValueError('Invalid Parameter type: "' + type(letra) + '", expected "str" or "Letra"!')

    def __getitem__(self, key):
        return self.obterIndices(key)

    def __str__(self):
        palavra = [''] * self.tamanho
        for letra in self._letraIndice:
            for idx in self.obterIndices(letra).indices:
                palavra[idx] = letra
        return ''.join(palavra)

    @property
    def tamanho(self):
        return self._tamanho

    @tamanho.setter
    def tamanho(self, value):
        assert isinstance(value, int)
        self._tamanho = value

    @property
    def letraIndice(self):
        return self._letraIndice

    def filho(self):
        letras = [''] * self.tamanho
        for letra_indice in self.letraIndice.values():
            for idx in letra_indice:
                letras[idx] = letra_indice.letra
        return letras
